package com.balwinderbadgal.vidpro.keys

object ExtrasKey{
    val VIDEO_ID="video_id"
    val LIKES="likes"
    val DIS_LIKES="dis_like"
    val VIEWS="views"
    val TITLE="title"
    val URL="url"
    val SUGGESTION_REQUEST_CODE=11
    val VIDEO_DATA="video_data"
    val THUMNAIL="thumnail"
}