package com.sachtech.manishpreet.advertisingapp.extension

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.LinearLayout
import com.sachtech.manishpreet.advertisingapp.R
import com.sachtech.manishpreet.advertisingapp.keys.PrefrencesKeys

/**
 * The `fragment` is added to the container view with id `frameId`. The operation is
 * performed by the `fragmentManager`.
 */
fun AppCompatActivity.replaceFragmentInActivity(fragment: Fragment, @IdRes frameId: Int) {
    supportFragmentManager.transact {
        replace(frameId, fragment)
    }
}

fun AppCompatActivity.addFragmentInActivity(fragment: Fragment, @IdRes frameId: Int) {
    supportFragmentManager.transact {
        add(frameId, fragment)
    }
}

/**
 * Runs a FragmentTransaction, then calls commit().
 */
private inline fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
    beginTransaction().apply {
        action()
    }.commit()
}


fun AppCompatActivity.setLayout(
    user_type: String,
    activeView: LinearLayout,
    activeImage: ImageView,
    inactive_one: LinearLayout,
    inactiveImageOne: ImageView,
    inactive_two: LinearLayout,
    inactiveImageTwo: ImageView,
    inactive_three: LinearLayout,
    inactiveImageThree: ImageView
) {

    setPrefrence(PrefrencesKeys.USER_TYPE, user_type)
    activeView.background = drawable(R.drawable.corner_background)
    activeImage.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN)

    inactive_one.background = drawable(R.drawable.white_background)
    inactiveImageOne.setColorFilter(
        ContextCompat.getColor(this, R.color.colorPrimary),
        android.graphics.PorterDuff.Mode.SRC_IN
    )
    inactive_two.background = drawable(R.drawable.white_background)
    inactiveImageTwo.setColorFilter(
        ContextCompat.getColor(this, R.color.colorPrimary),
        android.graphics.PorterDuff.Mode.SRC_IN
    )
    inactive_three.background = drawable(R.drawable.white_background)
    inactiveImageThree.setColorFilter(
        ContextCompat.getColor(this, R.color.colorPrimary),
        android.graphics.PorterDuff.Mode.SRC_IN
    )
}
