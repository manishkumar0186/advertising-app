package com.sachtech.manishpreet.advertisingapp.app

import android.app.Application

class App : Application() {
    val pref by lazy { getSharedPreferences("advertising", 0) }
    override fun onCreate() {
        super.onCreate()
        application = this
    }


    companion object {
        lateinit var application: App
    }


}