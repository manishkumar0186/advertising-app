package com.balwinderbadgal.vidpro.intracter

import okhttp3.ResponseBody
import retrofit2.Response

interface FormatGetter {
    fun formatMp3(response: Response<ResponseBody>)
    fun formatMp4(response: ResponseBody)
}