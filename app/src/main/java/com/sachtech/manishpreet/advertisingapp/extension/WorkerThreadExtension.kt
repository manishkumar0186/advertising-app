package com.sachtech.manishpreet.advertisingapp.extension


import android.os.Handler
import java.util.concurrent.TimeUnit

/**
 * Extension method to run block of code after specific Delay.
 */
fun runDelayed(delaySeconds: Long, timeUnit: TimeUnit = TimeUnit.SECONDS, action: () -> Unit) {
    Handler().postDelayed(action, timeUnit.toMillis(delaySeconds))
}
