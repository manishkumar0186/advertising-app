package com.sachtech.manishpreet.advertisingapp.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.sachtech.manishpreet.advertisingapp.R
import com.sachtech.manishpreet.advertisingapp.extension.*
import com.sachtech.manishpreet.advertisingapp.keys.RegisterUserKeys
import kotlinx.android.synthetic.main.activity_selection.*

class SelectionActivity : AppCompatActivity(), View.OnClickListener {

    var isSelected = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selection)


        //setlistner for views
        setListner()
    }


    private fun setListner() {

        layoutAdmin.setOnClickListener(this)
        layoutSeller.setOnClickListener(this)
        layoutCustomer.setOnClickListener(this)
        layoutPromoter.setOnClickListener(this)
        txtNext.setOnClickListener(this)
    }


    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.layoutAdmin -> {
                isSelected = true
                setLayout(
                    RegisterUserKeys.REGISTER_WITH_ADMIN,
                    layoutAdmin,
                    ivAdmin,
                    layoutSeller,
                    ivSeller,
                    layoutCustomer,
                    ivCustomer,
                    layoutPromoter,
                    ivPromoter
                )
            }
            R.id.layoutSeller -> {
                isSelected = true
                setLayout(
                    RegisterUserKeys.REGISTER_WITH_SELLER,
                    layoutSeller,
                    ivSeller,
                    layoutAdmin,
                    ivAdmin,
                    layoutCustomer,
                    ivCustomer,
                    layoutPromoter,
                    ivPromoter
                )
            }
            R.id.layoutCustomer -> {
                isSelected = true
                setLayout(
                    RegisterUserKeys.REGISTER_WITH_CUSTOMER,
                    layoutCustomer,
                    ivCustomer,
                    layoutSeller,
                    ivSeller,
                    layoutAdmin,
                    ivAdmin,
                    layoutPromoter,
                    ivPromoter
                )
            }
            R.id.layoutPromoter -> {
                isSelected = true
                setLayout(
                    RegisterUserKeys.REGISTER_WITH_PROMTER,
                    layoutPromoter,
                    ivPromoter,
                    layoutSeller,
                    ivSeller,
                    layoutCustomer,
                    ivCustomer,
                    layoutAdmin,
                    ivAdmin
                )
            }
            R.id.txtNext -> {

                var permission = arrayOf(
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                )

                if (hasPermissions(permission)) {
                    if (isSelected)
                        openActivity<LoginActivity>()
                    else
                        "select user type".toast()

                } else {
                    permission.withPermissions(this)
                }

            }
        }
    }

}