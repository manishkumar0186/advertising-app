package com.sachtech.manishpreet.advertisingapp.models.error

import com.google.gson.annotations.SerializedName

data class ErrorBody(
	@SerializedName("Status") val status: Boolean? = null,
	@SerializedName("Message") val message: String? = null
)
