package com.sachtech.manishpreet.advertisingapp.extension

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Handler
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.annotation.LayoutRes
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.sachtech.manishpreet.advertisingapp.app.App

/* fun toast(text: String?) {
     Toast.makeText(App.application, text, Toast.LENGTH_LONG).show()
 }*/

/**
 * Extension method to show toast for String.
 */
fun String.toast(isShortToast: Boolean = true) {
    if (isShortToast)
        Toast.makeText(App.application, this, Toast.LENGTH_SHORT).show()
    else
        Toast.makeText(App.application, this, Toast.LENGTH_LONG).show()
}


fun Array<String>.withPermissions(activity: Activity, body: (() -> Unit)? = null) {
    if (hasPermissions(this)) {
        body?.invoke()
    } else {
        askPermissions(activity)
        checkingPermissionsStatus(activity, body)
    }
}

private fun Array<String>.askPermissions(activity: Activity) {
    ActivityCompat.requestPermissions(
        activity,
        this,
        700
    )
}

private fun Array<String>.checkingPermissionsStatus(activity: Activity, body: (() -> Unit)?) {
    val maxCount = 20
    var count = 0

    val handler = Handler()
    var runnable: Runnable? = null
    runnable = Runnable {
        if (hasPermissions(this)) {
            body?.invoke()
            handler.removeCallbacksAndMessages(null)
            runnable = null
        }
        if (count > maxCount) {
            handler.removeCallbacksAndMessages(null)
            runnable = null
        }
        count += 1
        handler.postDelayed(runnable, 500)
    }
    handler.postDelayed(runnable, 500)
}

fun <T> T.hasPermissions(permissions: Array<String>): Boolean {
    var allPerm = true
    for (permission: String in permissions) {
        if (ContextCompat.checkSelfPermission(App.application, permission)
            != PackageManager.PERMISSION_GRANTED
        ) {
            allPerm = false
        }
    }
    return allPerm
}


inline fun <reified T> Context.openActivity(body: Intent.() -> Unit = {}) {
    val intent = Intent(App.application.applicationContext, T::class.java)
    intent.body()
    startActivity(intent)
}

operator fun List<Int>.times(by: Int): List<Int> {
    return this.map { it * by }
}

operator fun List<Int>.get(by: Int): List<Int> {
    return this.map { it * by }
}

fun isvalidate(email: String, pass: String): Boolean {
    if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        "enter valid e-mail".toast()
    else if (pass.length <= 6)
        "password should be grater than 6 chracter".toast()
    else
        return true

    return false
}

var dialog: ProgressDialog? = null
fun showDialog() {
    dialog = ProgressDialog(App.application)
    dialog!!.setCancelable(false)
    dialog!!.setCanceledOnTouchOutside(false)
    dialog!!.setMessage("Processing...")
    dialog!!.show()
}

fun hideDialog() {
    if (dialog != null)
        dialog!!.dismiss()
}

/**
 * Extension method to Get Color for resource for Context.
 */
fun color(@ColorRes id: Int) = ContextCompat.getColor(App.application, id)


/**
 * Extension method to Get Drawable for resource for Context.
 */
fun drawable(@DrawableRes id: Int) = ContextCompat.getDrawable(App.application, id)

/**
 * InflateLayout
 */
fun Context.inflateLayout(@LayoutRes layoutId: Int, parent: ViewGroup? = null, attachToRoot: Boolean = false): View =
    LayoutInflater.from(this).inflate(layoutId, parent, attachToRoot)
